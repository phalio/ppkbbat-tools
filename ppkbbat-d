#!/bin/bash

# PinePhone Keyboard battery power management daemon

# Copyright (C) 2022  Lily <alilybit@disroot.org>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# help
printhelp () {
    echo -e "\nUsage:\n"
    echo -e "ppkbbat-d [options] <interval>\n"
    echo -e "  <interval>  The update interval in seconds. Default is 1. Use 0 to run once.\n"
    echo -e "Options:\n"
    echo -e "  -q  Sequential charge mode (charge PP first, then KB), default is parallel charge mode."
    echo -e "  -s  Silent mode that prints no log messages.\n"
    echo -e "This daemon automatically manages the PinePhone’s and Keyboard’s battery input limits. You can run it manually in a terminal or have it run automatically in the background, e.g. as a systemd service. It also features two charge modes: Parallel (default) and sequential (-q). Refer to the readme of this repository for assistance.\n"
}
if [ "$1" = "-h" ] || [ "$1" = "--help" ] || [ "$1" = "help" ]; then
    printhelp
    exit 0
fi

# config file
. /etc/ppkbbat-tools 2> /dev/null

# if not defined in config file: default kb charge limits, work well with 5V 3A power supplies
if [ "$parallel_kb_charge_limit" = "" ]; then
    parallel_kb_charge_limit=1500000
fi
if [ "$sequential_kb_charge_limit" = "" ]; then
    sequential_kb_charge_limit=800000
fi
if [ "$pp_current_max" = "" ]; then
    pp_current_max=1500000
fi

# check for pp or ppp
if [ -d /sys/class/power_supply/axp20x-battery/ ]; then
    internal_battery=axp20x #pp
elif [ -d /sys/class/power_supply/rk818-battery/ ]; then
    internal_battery=rk818 #ppp
else
    printf "Could not recognize internal battery, exiting.\n"
    exit 1
fi

# pp/ppp current limits (unlike the kb than accepts arbitrary values, they only take a limited set of values and they are different for both devices)
# pp
if [ "$internal_battery" = "axp20x" ]; then
    pp_current_min=500000
    pp_current_med=900000
#ppp
elif [ "$internal_battery" = "rk818" ]; then
    pp_current_min=450000
    pp_current_med=850000
fi

# check for old or new kb directory
if [ -d /sys/class/power_supply/ip5xxx-battery ]; then
    kbpath=ip5xxx-battery
else
    kbpath=ip5xxx-charger
fi

# compatibility with the formerly bugged always positive og pp current value; this compatibility is currently active based on whether the old kernel driver is present, i'm not entirely sure but i think the current was fixed a bit after the new kernel driver so there might be a kernel version that has both the new driver and the broken pp current, in that case ppkbbat-d will not work properly, if you are in this situation tell me so i can fix it
if [ "$kbpath" = "ip5xxx-charger" ] && [ "$internal_battery" = "axp20x" ]; then
    broken_current=1
    le_fix="-ge"    # printf $le_chonk
    gt_fix="-lt"
    negative_fix=""
    truecurrent_operator_fix="+"
elif [ "$(cat /sys/class/power_supply/$internal_battery-battery/status)" = "Discharging" ] && [ $(cat /sys/class/power_supply/$internal_battery-battery/current_now) -ge 100000 ]; then    # this is supposed to recognise the case as described above of new kernel driver + bugged pp current
    printf "It looks like you are on a kernel version that has the new PPKB driver but not yet the fixed OG PP current value. ppkbbat-d is not yet compatible with this specific case. Please open an issue at https://codeberg.org/alilybit/ppkbbat-tools/issues or contact Lily via any of the methods from https://lilyb.it/links so that compatibility can be added."
    exit 1
else
    broken_current=0
    le_fix="-le"
    gt_fix="-gt"
    negative_fix="-"
    truecurrent_operator_fix="-"
fi

# get update interval from $1 or default to 1 second or run only once if 0
sleeptime=$(echo $@ | tr -dc '0-9')
if [ "$sleeptime" = "" ]; then
    sleeptime=1
elif [ "$sleeptime" = 0 ]; then
    run_once=1
else sleeptime=$sleeptime
fi

# values to set
pp_limit_set=/sys/class/power_supply/$internal_battery-usb/input_current_limit
kb_limit_set=/sys/class/power_supply/$kbpath/constant_charge_current
pp_online_set=/sys/class/power_supply/$internal_battery-battery/online
kb_online_set=/sys/class/power_supply/ip5xxx-boost/online

# high power demand default state
high_demand=0

# get readings
get_readings () {
    pp_limit=$(cat $pp_limit_set)
    kb_limit=$(cat $kb_limit_set 2> /dev/null)
    pp_current=$(cat /sys/class/power_supply/$internal_battery-battery/current_now)
    kb_current=$(cat /sys/class/power_supply/$kbpath/current_now 2> /dev/null)
    pp_capacity=$(cat /sys/class/power_supply/$internal_battery-battery/capacity)
    if [ ! $kb_current = 0 ]; then
	kb_capacity=$(cat /sys/class/power_supply/$kbpath/capacity 2> /dev/null)
    fi
    pp_status=$(cat /sys/class/power_supply/$internal_battery-battery/status)
    kb_status=$(cat /sys/class/power_supply/$kbpath/status 2> /dev/null)
    pp_online=$(cat $pp_online_set)
    kb_online=$(cat $kb_online_set 2> /dev/null)
    timestamp=$(date +%T)
}

# -q sequential charge mode (charge PP first, then KB), default is parallel charge mode
if [ ! "$(echo "$@" | tr -dc 'q')" = "" ]; then
    charge_mode=sequential
else
    charge_mode=parallel
fi

# -s silent (remove log messages)
if [ ! "$(echo "$@" | tr -dc 's')" = "" ]; then
    silent=1
fi

# explain current case with timestamp for logging
printinfo () {
    if [ "$silent" = "" ]; then
	printf "$timestamp $1\n"
    fi
}

# start loop
while true; do
    get_readings

    # true current of internal battery; the purpose of this is to deduct the additional current supplied during high demands from the current value used to determine whether or not more current is needed, otherwise supplying additional current would decrease the amount of current the pp uses and therefore make the script think there is no high demand anymore, resulting in constant switching between normal current supply and increased current supply
    if [ $high_demand = 0 ]; then
	pp_current_true=$pp_current
    elif [ $high_demand = 1 ]; then
	pp_current_true=$(($pp_current $truecurrent_operator_fix 400000))
    else
	printinfo "Unknown demand state, exiting."
	exit 1
    fi

    # pp is discharging when it shouldn't, this likely means that the power pin is not connected
    if [ "$pp_status" = "Discharging" ] && [ $pp_capacity -le 85 ] && [ "$kb_online" = "1" ]; then
	high_demand=0
	if [ ! $pp_limit = $pp_current_med ]; then
	    echo $pp_current_med > $pp_limit_set
	fi
	if [ ! $kb_limit = 2000000 ]; then
	    echo 2000000 > $kb_limit_set
	fi
	printinfo "Error#3: (Ignore this if it only happens for a few seconds!) The phone is not charging even though it should. This likely means that your pins are not connecting. Shut the phone off and try re-inserting it into the keyboard case with the camera+earpiece side first. Press down firmly along the edges and in the pin area; you should hear several loud clicks. If this doesn't work, you might have to shim your pins or readjust your existing shim."

    # kb battery output is deactivated in software (not entirely via the button), so nothing can be done, but prepare for charging just in case
    elif [ "$kb_online" = "0" ] && [ ! "$kb_current" = "" ]; then
	high_demand=0
	if [ ! $pp_limit = $pp_current_max ]; then
	    echo $pp_current_max > $pp_limit_set
	fi
	if [ ! $kb_limit = 500000 ]; then
	    echo 500000 > $kb_limit_set
	fi
	printinfo "KB battery output is currently deactivated"

    # discharging, pp low: keep pp above 25%
    elif [ "$kb_status" = "Discharging" ] && [ $pp_capacity -le 25 ]; then
	high_demand=0
	while [ "$kb_status" = "Discharging" ] && [ $pp_capacity -lt 35 ]; do
	    get_readings
	    if [ ! $pp_limit = $pp_current_max ]; then
		echo $pp_current_max > $pp_limit_set
	    fi
	    if [ ! $kb_limit = $parallel_kb_charge_limit ]; then
		echo $parallel_kb_charge_limit > $kb_limit_set 2> /dev/null
	    fi
	    printinfo "Discharging, PP low: Keep PP above 25%%"
	    if [ "$run_once" = 1 ]; then
		exit 0
	    fi
	    sleep $sleeptime
	done

    # discharging, high usage: medium kb->pp powering
    elif [ "$kb_status" = "Discharging" ] && [ $pp_current_true $le_fix ${negative_fix}600000 ]; then
	high_demand=1
	if [ ! $pp_limit = $pp_current_med ]; then
	    echo $pp_current_med > $pp_limit_set
	fi
	if [ ! $kb_limit = $parallel_kb_charge_limit ]; then
	    echo $parallel_kb_charge_limit > $kb_limit_set
	fi
	printinfo "Discharging, high usage: Medium KB->PP powering"

    # discharging, low usage: low kb->pp powering
    elif [ "$kb_status" = "Discharging" ] && [ $pp_current_true $gt_fix ${negative_fix}600000 ]; then
	high_demand=0
	if [ ! $pp_limit = $pp_current_min ]; then
	    echo $pp_current_min > $pp_limit_set
	fi
	if [ ! $kb_limit = $parallel_kb_charge_limit ]; then
	    echo $parallel_kb_charge_limit > $kb_limit_set
	fi
	printinfo "Discharging, low usage: Low KB->PP powering"

    # charging, high usage: max pp powering; only with non-broken current value since i don't know how to detect this state with the broken current value
    elif [ "$kb_status" = "Charging" ] && [ "$pp_status" = "Charging" ] && [ $broken_current = 0 ] && [ $pp_current_true -lt 0 ]; then
	high_demand=1
	if [ ! $pp_limit = $pp_current_max ]; then
	    echo $pp_current_max > $pp_limit_set
	fi
	if [ ! $kb_limit = 500000 ]; then
	    echo 500000 > $kb_limit_set
	fi
	printinfo "Charging, high usage: Max PP powering"

    # charging: charge both
    elif [ "$kb_status" = "Charging" ] && [ "$pp_status" = "Charging" ] && [ "$charge_mode" = "parallel" ]; then
	high_demand=0
	if [ ! $pp_limit = $pp_current_max ]; then
	    echo $pp_current_max > $pp_limit_set
	fi
	if [ ! $kb_limit = $parallel_kb_charge_limit ]; then
	    echo $parallel_kb_charge_limit > $kb_limit_set
	fi
	printinfo "Charging: Charge both"

    # charging: charge pp first
    elif [ "$kb_status" = "Charging" ] && [ "$pp_status" = "Charging" ] && [ $pp_capacity -lt 98 ]; then # full is determined as capacity -ge 98 since the last few percent accept very little current and take quite some time, so it would waste a lot of time to make the entire power available to the pp during this time and the little power from a 2A kb limit is enough to squeeze in those last 2%
	high_demand=0
	if [ ! $pp_limit = $pp_current_max ]; then
	    echo $pp_current_max > $pp_limit_set
	fi
	if [ ! $kb_limit = $sequential_kb_charge_limit ]; then
	    echo $sequential_kb_charge_limit > $kb_limit_set
	fi
	printinfo "Charging: Charge PP first"

    # charging, pp full, high usage: charge kb a bit
    elif [ "$kb_status" = "Charging" ] && [ $pp_current_true $le_fix ${negative_fix}600000 ]; then
	high_demand=1
	if [ ! $pp_limit = $pp_current_max ]; then
	    echo $pp_current_max > $pp_limit_set
	fi
	if [ ! $kb_limit = $sequential_kb_charge_limit ]; then
	    echo $sequential_kb_charge_limit > $kb_limit_set
	fi
	printinfo "Charging, PP full, high usage: Charge KB a bit"

    # charging, pp full, low usage: charge kb
    elif [ "$kb_status" = "Charging" ] && [ $pp_current_true $gt_fix ${negative_fix}600000 ]; then
	high_demand=0
	if [ ! $pp_limit = $pp_current_max ]; then
	    echo $pp_current_max > $pp_limit_set
	fi
	if [ ! $kb_limit = 2000000 ]; then
	    echo 2000000 > $kb_limit_set
	fi
	printinfo "Charging, PP full, low usage: Charge KB"

    # charging, kb full: charge pp
    elif [ "$kb_status" = "Full" ]; then
	high_demand=0
	if [ ! $pp_limit = $pp_current_max ]; then
	    echo $pp_current_max > $pp_limit_set
	fi
	if [ ! $kb_limit = 500000 ]; then
	    echo 500000 > $kb_limit_set
	fi
	printinfo "Charging, KB full: Charge PP"

    # kb off/empty: be ready for power supply
    elif [ "$kb_current" = "" ] && [ ! "$pp_status" = "Charging" ]; then
	high_demand=0
	if [ ! $pp_limit = $pp_current_max ]; then
	    echo $pp_current_max > $pp_limit_set
	fi
	printinfo "KB off/empty: Ready for power supply"

    # kb not reporting values when it should
    elif [ "$kb_current" = "" ] && [ "$pp_status" = "Charging" ]; then
	high_demand=0
	if [ ! $pp_limit = $pp_current_max ]; then
	    echo $pp_current_max > $pp_limit_set
	fi
	printinfo "Error#1: The keyboard has stopped reporting values and stopped charging if connected to a power supply. If this happens regularly to you, use ppkbbat-d as described in https://codeberg.org/alilybit/ppkbbat-tools#user-content-calling-ppkbbat-d-once-periodically if you aren't already. Consult https://codeberg.org/alilybit/ppkbbat-tools#error-1-error-2-charging-stops-when-plugged-in for further help."

    # "not charging" bug where the kb stops accepting power until replugged
    elif [ "$kb_status" = "Not charging" ]; then
	high_demand=0
	printinfo "Error#2: The keyboard has stopped charging. If this happens regularly to you, use ppkbbat-d as described in https://codeberg.org/alilybit/ppkbbat-tools#user-content-calling-ppkbbat-d-once-periodically if you aren't already. Consult https://codeberg.org/alilybit/ppkbbat-tools#error-1-error-2-charging-stops-when-plugged-in for further help."

    # else fall back to medium values that should provide minimum functionality in all states
    else
	high_demand=0
	if [ ! $pp_limit = $pp_current_max ]; then
	    echo $pp_current_max > $pp_limit_set
	fi
	if [ ! $kb_limit = $sequential_kb_charge_limit ]; then
	    echo $sequential_kb_charge_limit > $kb_limit_set 2> /dev/null
	fi
	printinfo "Unknown state: Falling back to medium values that should provide minimum functionality in all states"
    fi

    # update interval
    if [ "$run_once" = 1 ]; then
	exit 0
    fi
    sleep $sleeptime
done
